const { buildBabelConfig } = require('./config/babel-config-builder')

module.exports = buildBabelConfig(true, false)
