
import * as React from 'react'
import { Provider } from 'mobx-react'

import { ColorProp, color } from './colorStore'

interface Props {
  children?: React.ReactNode,
}

type ProviderProps =
  & Required<ColorProp>

const stores: ProviderProps = {
  color,
}

const MobxProvider = ({ children }: Props) => (
  <Provider {...stores}>
    {children}
  </Provider>
)

export default MobxProvider
