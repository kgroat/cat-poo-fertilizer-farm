
import * as React from 'react'

import { classes } from 'helpers/classNames'

import { icon, svgIcon } from './Icon.pcss'

export type IconName = keyof typeof iconMap

interface Props {
  name: IconName
  className?: string
  style?: React.CSSProperties
}

function makeSvgIcon (iconUrl: string, { style = {} }: { style?: React.CSSProperties } = {}) {
  return (
    <img
      src={iconUrl}
      className={svgIcon}
      style={style}
      // if all svgs are gona be nested in a button, cool
      // if not, these a11y attrs may need to change
      aria-hidden='true'
      role='presentation'
    />
  )
}

export const iconMap = {
  'star': makeSvgIcon(require('./icons/filled-star.svg')),
  'empty-star': makeSvgIcon(require('./icons/empty-star.svg')),
  'half-star': makeSvgIcon(require('./icons/half-star.svg')),

  'user': makeSvgIcon(require('./icons/user.svg')),
  'menu': makeSvgIcon(require('./icons/menu.svg')),
  'edit': makeSvgIcon(require('./icons/edit.svg')),
  'close': makeSvgIcon(require('./icons/close.svg')),
  'check': makeSvgIcon(require('./icons/check.svg')),
  'search': makeSvgIcon(require('./icons/search.svg')),

  'chevron-up': makeSvgIcon(require('./icons/chevron.svg')),
  'chevron-down': makeSvgIcon(require('./icons/chevron.svg'), { style: { transform: 'rotate(180deg)' } }),
  'chevron-left': makeSvgIcon(require('./icons/chevron.svg'), { style: { transform: 'rotate(-90deg)' } }),
  'chevron-right': makeSvgIcon(require('./icons/chevron.svg'), { style: { transform: 'rotate(90deg)' } }),

  'npm': makeSvgIcon(require('./icons/npm.svg')),
  'github': makeSvgIcon(require('./icons/github.svg')),

  'siu': makeSvgIcon(require('./icons/siu.svg')),

  'litter-box': makeSvgIcon(require('./icons/litter_box.svg')),

  'poos/bodies/gold': makeSvgIcon(require('./poos/bodies/gold.svg')),
  'poos/bodies/platinum': makeSvgIcon(require('./poos/bodies/platinum.svg')),
  'poos/bodies/rainbow': makeSvgIcon(require('./poos/bodies/rainbow.svg')),
  'poos/bodies/standard': makeSvgIcon(require('./poos/bodies/standard.svg')),

  'poos/eyes/happy': makeSvgIcon(require('./poos/eyes/happy.svg')),
  'poos/eyes/nn': makeSvgIcon(require('./poos/eyes/nn.svg')),
  'poos/eyes/open': makeSvgIcon(require('./poos/eyes/open.svg')),
  'poos/eyes/shut-tight': makeSvgIcon(require('./poos/eyes/shut _tight.svg')),
  'poos/eyes/sparkle-1-1': makeSvgIcon(require('./poos/eyes/sparkle_1_1.svg')),
  'poos/eyes/sparkle-1-2': makeSvgIcon(require('./poos/eyes/sparkle_1_2.svg')),
  'poos/eyes/sparkle-2-1': makeSvgIcon(require('./poos/eyes/sparkle_2_1.svg')),
  'poos/eyes/sparkle-2-2': makeSvgIcon(require('./poos/eyes/sparkle_2_2.svg')),
  'poos/eyes/sparkle-2-3': makeSvgIcon(require('./poos/eyes/sparkle_2_3.svg')),
  'poos/eyes/sparkle-2-4': makeSvgIcon(require('./poos/eyes/sparkle_2_4.svg')),
  'poos/eyes/xx': makeSvgIcon(require('./poos/eyes/xx.svg')),

  'poos/mouths/frown': makeSvgIcon(require('./poos/mouths/frown.svg')),
  'poos/mouths/o-1': makeSvgIcon(require('./poos/mouths/o_1.svg')),
  'poos/mouths/o-2': makeSvgIcon(require('./poos/mouths/o_2.svg')),
  'poos/mouths/p-frown': makeSvgIcon(require('./poos/mouths/p frown.svg')),
  'poos/mouths/p-smile': makeSvgIcon(require('./poos/mouths/p_smile.svg')),
  'poos/mouths/p': makeSvgIcon(require('./poos/mouths/p.svg')),
  'poos/mouths/smile_open': makeSvgIcon(require('./poos/mouths/smile_open.svg')),
  'poos/mouths/smile_teeth': makeSvgIcon(require('./poos/mouths/smile_teeth.svg')),
  'poos/mouths/smile': makeSvgIcon(require('./poos/mouths/smile.svg')),
}

const im = {
  ...iconMap,
}

export function Icon ({ name, className, style }: Props) {
  return <i className={classes([icon, className])} style={style}>{im[name]}</i>
}
