
export { Icon, iconMap } from './Icon'
import { IconName } from './Icon'
export type IconName = IconName
