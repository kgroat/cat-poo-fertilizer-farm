import * as React from 'react'
import { storiesOf } from '@storybook/react'
import { select, withKnobs, text, number } from '@storybook/addon-knobs'
import Icon, { IconName, iconMap } from './Icon'

const possibleIcons = Object.keys(iconMap) as IconName[]

storiesOf('Icon', module)
  .addDecorator(withKnobs)
  .add('Default', () => (
    <Icon name={select<IconName>('name', possibleIcons, 'star')} />
  ))
  .add('With knobs', () => (
    <div style={{
      color: text('color', '#a43762'),
      backgroundColor: text('background', '#cccccc'),
      fontSize: `${number('size', 3, { range: true, min: 1, max: 10, step: 0.1 })}em`,
    }}>
      These ->{' '}
      <Icon name={select<IconName>('name', possibleIcons, 'star')} />
      <Icon name={select<IconName>('name', possibleIcons, 'star')} />
      <Icon name={select<IconName>('name', possibleIcons, 'star')} />
      {' '} are icons
    </div>
  ))
