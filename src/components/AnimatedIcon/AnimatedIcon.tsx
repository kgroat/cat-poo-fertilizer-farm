
import * as React from 'react'

import { Icon, IconName } from '../Icon'

interface Props {
  names: IconName | IconName[]
  frequency?: number
  className?: string
  style?: React.CSSProperties
}

export function AnimatedIcon ({ names: nameOrNames, frequency = 150, className, style }: Props) {
  const names = nameOrNames instanceof Array ? nameOrNames : [nameOrNames]
  const [intervalId, setIntervalId] = React.useState<NodeJS.Timeout | null>(null)
  const [frame, setFrame] = React.useState<number>(0)

  React.useEffect(() => {
    setFrame(0)

    if (intervalId !== null) {
      clearInterval(intervalId)
    }

    if (names.length > 1) {
      let i = 0
      setIntervalId(setInterval(() => {
        i = (i + 1) % names.length
        setFrame(i)
      }, frequency))
    }
  }, [nameOrNames, frequency])

  return (
    <Icon name={names[frame % names.length]} className={className} style={style} />
  )
}
