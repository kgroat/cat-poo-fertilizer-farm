import { Eyes, Mouth } from './PooParts'

export interface Face {
  eyes: Eyes
  mouth: Mouth
}

export const faces: Face[] = [
  {
    eyes: 'nn',
    mouth: 'o-animated',
  },
  {
    eyes: 'nn',
    mouth: 'smile',
  },
  {
    eyes: 'happy',
    mouth: 'smile',
  },
  {
    eyes: 'happy',
    mouth: 'p-smile',
  },
  {
    eyes: 'sparkle-1',
    mouth: 'smile-open',
  },
  {
    eyes: 'sparkle-1',
    mouth: 'frown',
  },
  {
    eyes: 'shut-tight',
    mouth: 'frown',
  },
  {
    eyes: 'sparkle-2',
    mouth: 'smile-open',
  },
  {
    eyes: 'sparkle-2',
    mouth: 'o',
  },
  {
    eyes: 'open',
    mouth: 'smile-open',
  },
  {
    eyes: 'open',
    mouth: 'smile-teeth',
  },
  {
    eyes: 'shut-tight',
    mouth: 'smile-teeth',
  },
  {
    eyes: 'shut-tight',
    mouth: 'smile',
  },
  {
    eyes: 'shut-tight',
    mouth: 'smile-open',
  },
  {
    eyes: 'shut-tight',
    mouth: 'p',
  },
  {
    eyes: 'open',
    mouth: 'p',
  },
  {
    eyes: 'shut-tight',
    mouth: 'p-smile',
  },
  {
    eyes: 'shut-tight',
    mouth: 'p-frown',
  },
  {
    eyes: 'xx',
    mouth: 'p-frown',
  },
  {
    eyes: 'xx',
    mouth: 'o',
  },
]
