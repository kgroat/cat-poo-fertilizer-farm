
import * as React from 'react'

import { IconName } from '../Icon'
import { poo__face } from './Poo.pcss'
import { AnimatedIcon } from 'components/AnimatedIcon'

type IconsWithSpeed = { frequency: number, icons: IconName[] }
type IconNames = IconName | IconName[] | IconsWithSpeed

export type Eyes =
    'happy'
  | 'nn'
  | 'open'
  | 'shut-tight'
  | 'sparkle-1'
  | 'sparkle-2'
  | 'xx'

const eyeMap: { [key in Eyes]: IconNames } = {
  'happy': 'poos/eyes/happy',
  'nn': 'poos/eyes/nn',
  'open': 'poos/eyes/open',
  'shut-tight': 'poos/eyes/shut-tight',
  'sparkle-1': ['poos/eyes/sparkle-1-1', 'poos/eyes/sparkle-1-2'],
  'sparkle-2': ['poos/eyes/sparkle-2-1', 'poos/eyes/sparkle-2-2', 'poos/eyes/sparkle-2-3', 'poos/eyes/sparkle-2-4'],
  'xx': 'poos/eyes/xx',
}

export type Mouth =
    'frown'
  | 'o'
  | 'o-animated'
  | 'p-frown'
  | 'p-smile'
  | 'p'
  | 'smile-open'
  | 'smile-teeth'
  | 'smile'

const mouthMap: { [key in Mouth]: IconNames } = {
  'frown': 'poos/mouths/frown',
  'o': 'poos/mouths/o-1',
  'o-animated': { frequency: 500, icons: ['poos/mouths/o-1', 'poos/mouths/o-2'] },
  'p-frown': 'poos/mouths/p-frown',
  'p-smile': 'poos/mouths/p-smile',
  'p': 'poos/mouths/p',
  'smile-open': 'poos/mouths/smile_open',
  'smile-teeth': 'poos/mouths/smile_teeth',
  'smile': 'poos/mouths/smile',
}

export type Body =
    'gold'
  | 'platinum'
  | 'rainbow'
  | 'standard'

const bodyMap: { [key in Body]: IconNames } = {
  'gold': 'poos/bodies/gold',
  'platinum': 'poos/bodies/platinum',
  'rainbow': 'poos/bodies/rainbow',
  'standard': 'poos/bodies/standard',
}

export interface PooEyesProps {
  name: Eyes
}

function hasSpeed (icons: IconNames): icons is IconsWithSpeed {
  return typeof icons !== 'string' && !(icons instanceof Array)
}

function getIconsAndSpeed (icons: IconNames) {
  return hasSpeed(icons) ? icons : { icons, frequency: undefined }
}

export function PooEyes ({ name }: PooEyesProps) {
  const { icons, frequency } = getIconsAndSpeed(eyeMap[name])
  return <AnimatedIcon names={icons} frequency={frequency} className={poo__face} />
}

export interface PooMouthProps {
  name: Mouth
}

export function PooMouth ({ name }: PooMouthProps) {
  const { icons, frequency } = getIconsAndSpeed(mouthMap[name])
  return <AnimatedIcon names={icons} frequency={frequency} className={poo__face} />
}

export interface PooBodyProps {
  name: Body
}

export function PooBody ({ name }: PooBodyProps) {
  const { icons, frequency } = getIconsAndSpeed(bodyMap[name])
  return <AnimatedIcon names={icons} frequency={frequency} />
}
