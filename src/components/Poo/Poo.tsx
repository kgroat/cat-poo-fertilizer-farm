
import * as React from 'react'

import { Body, PooBody, PooEyes, PooMouth } from './PooParts'
import { poo } from './Poo.pcss'
import { Face, faces } from './faces'

function chooseRandom<T> (arr: T[]): T {
  return arr[Math.floor(Math.random() * arr.length)]
}

export interface PooProps extends Partial<Face> {
  body?: Body
}

function randomBody (): Body {
  const body = Math.floor(Math.random() * 4)
  switch (body) {
    case 0: return 'gold'
    case 1: return 'platinum'
    case 2: return 'rainbow'
    default: return 'standard'
  }
}

export function Poo (props: PooProps) {
  const [defaultBody] = React.useState(randomBody())
  const [defaultFace] = React.useState(chooseRandom(faces))

  const { eyes, body, mouth }: PooProps = { ...defaultFace, body: defaultBody, ...props }

  return (
    <div className={poo} style={{ fontSize: '20rem' }}>
      <PooBody name={body} />
      <PooEyes name={eyes} />
      <PooMouth name={mouth} />
    </div>
  )
}
