
import { hot, setConfig } from 'react-hot-loader'
import { logLevel } from 'helpers/logging'

import { App as OrigApp } from './App'

setConfig({
  logLevel,
})

export const App = hot(module)(OrigApp)
