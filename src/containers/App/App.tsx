
import * as React from 'react'

import { Poo } from 'components/Poo'

const POO_COUNT = 20

export function App () {
  return (
    <React.Fragment>
      {
        Array.from(Array(POO_COUNT)).map((_, idx) => <Poo key={idx} />)
      }
    </React.Fragment>
  )
}
