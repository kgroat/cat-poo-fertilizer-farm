
import * as React from 'react'
import { shallow } from 'enzyme'

import { App } from './App'

jest.mock('state/MobxProvider')

describe(App.name, () => {
  it('should mount without throwing an error', () => {
    expect(shallow(<App />)).toMatchSnapshot()
  })
})
