
export type LogLevel = 'debug' | 'log' | 'warn' | 'error'
export const DEFAULT_LOG_LEVEL: LogLevel = 'warn'

enum LogLevelIdx {
  debug = 0,
  log = 1,
  warn = 2,
  error = 3,
}

export const logLevel = Object.keys(LogLevelIdx).includes(process.env.LOG_LEVEL!)
  ? process.env.LOG_LEVEL as LogLevel
  : DEFAULT_LOG_LEVEL

const logLevelIdx = LogLevelIdx[logLevel]

const noop = () => undefined
function buildLogger (logger: (...args: any) => void, level: LogLevel) {
  if (logLevelIdx <= LogLevelIdx[level]) {
    return logger
  } else {
    return noop
  }
}

export const debug = buildLogger(console.debug, 'debug')
export const log = buildLogger(console.log, 'log')
export const warn = buildLogger(console.warn, 'warn')
export const error = buildLogger(console.error, 'error')
