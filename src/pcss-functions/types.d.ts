
import { HSLa } from './toHsl'
import { Rgba } from './toRgb'
import { Srgba } from './toSRgb'

export type Color = HSLa | Rgba | Srgba | string
