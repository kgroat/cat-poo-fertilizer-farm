
import './global-css/index.pcss'

import * as React from 'react'
import { render } from 'react-dom'
import 'roboto-fontface/css/roboto/roboto-fontface.css'

import { App } from 'containers/App'

render(<App />, document.getElementById('app'))
