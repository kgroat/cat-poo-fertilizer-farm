
const npsUtils = require('nps-utils')

const jestConfig = require.resolve('./config/jest/config.js')
const webpackConfig = require.resolve('./config/webpack/config.ts')
const onlyStyleTypesWebpackConfig = './config/webpack/onlyStyleTypes.ts'

const webpackTsconfig = `TS_NODE_PROJECT=${require.resolve('./config/tsconfig.node.json')}`
const webpack = `${webpackTsconfig} NODE_ENV="production" webpack`
const webpackDevServer = `${webpackTsconfig} webpack-dev-server`

const CI = process.env.CI

function addForCI (cmd, toAdd) {
  return CI ? `${cmd} ${toAdd}` : cmd
}

function ifServerStarted (ifStarted, ifNotStarted) {
  if (CI) {
    return ifNotStarted
  }

  return `
    if curl -s "http://localhost:\${PORT:-3000}" >/dev/null; then
      ${ifStarted}
    else
      ${ifNotStarted}
    fi
  `
}

module.exports = {
  scripts: {
    default: {
      script: 'nps webpack',
      description: 'Starts webpack dev server.',
    },
    webpack: {
      script: `${webpackDevServer} --config ${webpackConfig} --env.dev`,
      description: 'Starts webpack dev server.',
    },
    build: {
      script: 'nps build.clean build.app',
      description: 'Builds a production bundle of the application.',
      clean: {
        script: `rm -rf 'public'`,
        description: 'removes the output directory'
      },
      app: {
        script: `${webpack} --config ${webpackConfig}`,
        description: 'bundles the app',
      },
    },
    test: {
      script: `jest --config ${jestConfig}`,
      description: 'Starts the jest tests for the application.',
      fix: {
        script: `nps "test -u"`,
        description: 'Runs the tests in snapshot-update mode.',
      }
    },
    lint: {
      script: 'nps lint.ts lint.css',
      description: 'Runs tslint and stylelint.',
      ts: {
        script: 'tslint --project .',
        description: 'Runs tslint for the entire project.',
        fix: {
          script: 'nps "lint.ts --fix"',
          description: 'Runs tslint in fix mode.',
        },
      },
      css: {
        script: 'stylelint "src/**/*.{pcss,css}"',
        description: 'Runs stylelint for the entire project.',
        fix: {
          script: 'nps "lint.css --fix"',
          description: 'Runs tslint in fix mode.',
        },
      },
      fix: {
        script: 'nps lint.ts.fix lint.css.fix',
        description: 'Runs tslint and stylelint in fix mode.',
      },
    },
    check: {
      script: 'nps build.app lint test',
      description: 'Runs the checks to validate the application (test & lint).',
    },
    css: {
      types: {
        script: `${webpack} --config ${onlyStyleTypesWebpackConfig}`,
        description: 'Generates typescript typings for css modules.',
        watch: {
          script: 'nps "css.types -w"',
          description: 'Generates typescript typings for css modules in watch mode.',
        },
      },
    },
  },
}
