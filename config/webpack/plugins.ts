
import * as webpack from 'webpack'
import * as path from 'path'

import * as HtmlWebpackPlugin from 'html-webpack-plugin'
import * as MiniCssExtractPlugin from 'mini-css-extract-plugin'
import * as CaseSensitivePathsPlugin from 'case-sensitive-paths-webpack-plugin'
import * as CopyWebpackPlugin from 'copy-webpack-plugin'
import { InjectManifest } from 'workbox-webpack-plugin'
import * as StylelintPlugin from 'stylelint-webpack-plugin'
import { WatchMissingNodeModulesPlugin } from './WatchMissingNodeModulesPlugin'

import { LogLevel } from '../../src/helpers/logging'

import { Constants } from './constants'

export function buildPlugins (dev: boolean, constants: Constants, loglevel: LogLevel) {
  let plugins = [
    new HtmlWebpackPlugin({
      inject: 'body',
      template: constants.htmlPath,
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
      'process.env.LOG_LEVEL': JSON.stringify(loglevel),
      '__DEV__': JSON.stringify(dev),
    }),
    new StylelintPlugin({
      files: ['src/**/*.pcss'],
      configFile: path.join(__dirname, '../../.stylelintrc'),
    }),
  ]

  if (dev) {
    plugins = [
      ...plugins,
      new webpack.HotModuleReplacementPlugin(),
      new CaseSensitivePathsPlugin(),
      new WatchMissingNodeModulesPlugin('node_modules'),
    ]
  } else {
    plugins = [
      new CopyWebpackPlugin([
        {
          from: path.join(__dirname, '../../static'),
          to: constants.outputDir,
        },
      ]),
      ...plugins,
      new MiniCssExtractPlugin({
        filename: constants.outputCssFile,
        chunkFilename: constants.outputCssChunkFiles,
      }),
      new InjectManifest({
        importWorkboxFrom: 'disabled',
        swSrc: path.join(__dirname, '../../static/service-worker.js'),
      }),
    ]
  }

  return plugins
}
