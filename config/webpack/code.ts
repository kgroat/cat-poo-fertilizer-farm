import * as webpack from 'webpack'

const { buildBabelConfig } = require('../babel-config-builder')

export const codeRule = (dev: boolean, docgen = false): webpack.RuleSetRule => ({
  test: /\.(js|jsx|ts|tsx)$/,
  exclude: /node_modules/,
  use: [
    {
      loader: 'babel-loader',
      options: {
        compact: true,
        cacheDirectory: true,
        babelrc: false,
        ...buildBabelConfig(dev, docgen),
      },
    },
    docgen ? 'react-docgen-typescript-loader' : '',
  ].filter(l => l),
})
