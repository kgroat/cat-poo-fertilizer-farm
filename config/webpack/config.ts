import * as webpack from 'webpack'

import * as path from 'path'
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin'
// import * as UglifyJsPlugin from 'uglifyjs-webpack-plugin'
import * as OptimizeCssAssetsPlugin from 'optimize-css-assets-webpack-plugin'

import { LogLevel, DEFAULT_LOG_LEVEL } from '../../src/helpers/logging'

import { codeRule } from './code'
import { styleRule } from './styles'
import { buildConstants } from './constants'
import { buildPlugins } from './plugins'

interface WebpackEnv {
  dev?: boolean
  loglevel?: LogLevel
}

function createMainConfig ({ dev = false, loglevel = DEFAULT_LOG_LEVEL }: WebpackEnv = {}): webpack.Configuration & { devServer: { [k: string]: any } } {
  const constants = buildConstants(dev)

  return {
    bail: false,
    mode: dev ? 'development' : 'production',
    devtool: dev ? 'eval-source-map' : undefined,
    entry: [
      require.resolve('babel-polyfill'),
      require.resolve('../polyfills'),
      constants.entrypoint,
    ],
    devServer: {
      host: '0.0.0.0',
      port: process.env.PORT ? parseInt(process.env.PORT, 10) : 3000,
      contentBase: [
        constants.staticDir,
      ],
      compress: true,
      clientLogLevel: 'none',
      watchContentBase: true,
      hot: true,
      historyApiFallback: true,
      // TODO: remove this once this issue is resolved:
      // https://github.com/webpack/webpack-dev-server/issues/1604
      disableHostCheck: true,
    },
    output: {
      path: constants.outputDir,
      pathinfo: dev,
      publicPath: '/',
      filename: 'static/js/[name].[hash:8].js',
      chunkFilename: 'static/js/[name].[chunkhash:8].chunk.js',
      // Point sourcemap entries to original disk location (format as URL on Windows)
      devtoolModuleFilenameTemplate: dev
        ? (info: any) =>
            path
              .resolve(info.absoluteResourcePath)
              .replace(/\\/g, '/')
        : (info: any) =>
            path
              .relative(constants.srcDir, info.absoluteResourcePath)
              .replace(/\\/g, '/'),
    },
    resolve: {
      extensions: [
        '.ts',
        '.tsx',
        '.js',
        '.jsx',
        '.json',
      ],
      plugins: [
        new TsconfigPathsPlugin({ configFile: constants.tsconfig }),
      ],
      alias: dev ? {
        'react-dom': '@hot-loader/react-dom',
      } : undefined,
    },
    module: {
      strictExportPresence: true,
      rules: [
        {
          oneOf: [
            codeRule(dev),
            styleRule(dev),
            {
              exclude: /\.(js|jsx|mjs|ts|tsx|html|json|css|pcss)$/,
              loader: require.resolve('file-loader'),
              options: {
                name: 'static/media/[name].[hash:8].[ext]',
              },
            },
          ],
        },
      ],
    },
    plugins: buildPlugins(dev, constants, loglevel),
    node: {
      dgram: 'empty',
      fs: 'empty',
      net: 'empty',
      tls: 'empty',
      child_process: 'empty',
    },
    performance: {
      hints: dev ? false : 'warning',
    },
    optimization: !dev ? {
      minimizer: [
        // new UglifyJsPlugin({
        //   uglifyOptions: {
        //     output: {
        //       beautify: false,
        //       indent_level: 2,
        //     },
        //   },
        //   parallel: true,
        //   cache: 'uglify-cache',
        //   sourceMap: false,
        // }),
        new OptimizeCssAssetsPlugin({}),
      ],
    } : {
      namedModules: true,
    },
  }
}

export default createMainConfig
