import { addParameters, configure, addDecorator } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import { themes } from '@storybook/theming'

import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '../../src/global-css/colors.pcss'
import './global-styles.pcss'

const stories = require.context('../../src', true, /\.stories\.tsx$/)

function loadStories () {
  stories.keys().forEach(fileName => stories(fileName))
}

addParameters({
  options: {
    name: 'SIU',
    addonPanelInRight: true,
    theme: themes.dark,
  },
})

addDecorator(withInfo({
  inline: true,
  styles: (style) => {
    return ({
      ...style,
      infoBody: {
        ...style.infoBody,
        background: 'inherit',
        color: 'inherit',
        border: '1px solid rgba(128, 128, 128, 0.5)',
      },
    })
  },
}))

configure(loadStories, module)
