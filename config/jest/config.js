
module.exports = {
  rootDir: '../..',
  testEnvironment: 'jsdom',
  collectCoverage: true,
  collectCoverageFrom: [
    'src/**/*.{js,jsx,ts,tsx}',
    '!**/*.d.ts',
    '!**/*.stories.tsx',
    '!**/index.dev.tsx',
    '!**/index.tsx',
    '!**/index.ts',
  ],
  setupFiles: [
    '<rootDir>/config/jest/setup.js',
  ],
  testMatch: [
    '<rootDir>/src/**/*.test.(j|t)s?(x)',
  ],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  testURL: 'http://localhost',
  transform: {
    '^.+\\.[tj]sx?$': 'babel-jest',
    '^.+\\.p?css$': '<rootDir>/config/jest/cssTransform.js',
    '^(?!.*\\.([tj]sx?|p?css|json)$)': '<rootDir>/config/jest/fileTransform.js',
  },
  moduleDirectories: [
    'node_modules', '<rootDir>/src'
  ],
  transformIgnorePatterns: [
    '[/\\\\]node_modules[/\\\\].+\\.(js|jsx|ts|tsx)$',
  ],
  moduleFileExtensions: [
    'js',
    'jsx',
    'ts',
    'tsx',
    'json',
  ],
  globals: {
    '__DEV__': true,
  },
  coverageThreshold: {
    global: {
      statements: 1,
      branches: 1,
      lines: 1,
      functions: 1,
    },
  },
}
