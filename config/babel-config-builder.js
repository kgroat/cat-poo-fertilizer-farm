exports.buildBabelConfig = function (dev, docgen) {
  return {
    presets: [
      '@babel/preset-env',
      '@babel/preset-typescript',
      '@babel/preset-react',
    ],
    plugins: [
      ['@babel/plugin-proposal-decorators', { legacy: true }],
      ['@babel/plugin-proposal-class-properties', { loose: true }],
      docgen && ['react-docgen', {
        resolver: 'findAllComponentDefinitions',
      }],
      dev && 'react-hot-loader/babel',
    ].filter(p => p),
  }
}